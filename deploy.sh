#!/bin/sh

echo ${CI_COMMIT_SHA:0:8}

if [ -z $CI_ENVIRONMENT_NAME ]; then
  echo 'general env'
else
  echo $CI_ENVIRONMENT_NAME
fi
